Structure
 - PageObject is the Module to define WebElement and Actions on Page Object
 - Test Case is the Module to contain Test Case, Test Plan and Test Suite
 - Test Cases are contained in com.qa.automation.utest.testCase package
   + Depends on Feature or Web Page, we will push test cases into sub-package for managing. 

Build in Future
 - Build up test plan
 - Execute test suite via xml file
 - Regression via Jenkins
 
Manual Test Case
 - Manual test cases is stored in SignUpTestCase.7z
 