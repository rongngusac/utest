package com.qa.automation.utest.pageObjectsAction;

import com.qa.automation.utest.pageObjects.UTestSignUpPage;
import org.openqa.selenium.WebDriver;

public class UTestSignUpPageAction extends UTestSignUpPage {

    public UTestSignUpPageAction(WebDriver webDriver) {
        super(webDriver);
    }

    public void inputFirstName(String firstName) {
        inputFirstNameValue(firstName);
    }

    public void inputLastName(String lastName) {
        inputLastNameValue(lastName);
    }

    public void inputEmail(String email) {
        inputEmailValue(email);
    }

    public void selectDateOfBirth(String month, int date, int year) {
        selectMonth(month);
        selectDay(date);
        selectYear(year);
    }

    public void clickOnNextLocation() {
        clickNextLocationButton();
    }

    private void selectMonth(String month) {
        clickOnMonthDropdownBox();
        selectMonthDropDownValue(month);
    }

    private void selectDay(int date) {
        clickOnDayDropdownBox();
        selectDayDropDownValue(date);
    }

    private void selectYear(int year) {
        clickOnYearDropdownBox();
        selectYearDropDownValue(year);
    }

}
