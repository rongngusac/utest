package com.qa.automation.utest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebTestDriver {
    public WebDriver driver;

    public WebTestDriver(WebDriver webDriver) {
        this.driver = webDriver;
    }

    private WebElement getElement(String xPath) {
        WebElement webElement = driver.findElement(By.xpath(xPath));
        return webElement;
    }

    public void inputText(String xPath, String input) {
        WebElement webElement = getElement(xPath);
        webElement.sendKeys(input);
    }

    public void clickControl(String xPath) {
        WebElement webElement = getElement(xPath);
        webElement.click();
    }

    public boolean isElementPresent(String xPath) throws Exception {
        boolean isPresent = true;
        try {
            getElement(xPath);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return isPresent;

    }
}
