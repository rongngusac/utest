package com.qa.automation.utest.pageObjects;

import com.qa.automation.utest.WebTestDriver;
import org.openqa.selenium.WebDriver;

public class UTestSignUpPage {
    WebTestDriver webTestDriver;

    private static final String firstNameTextBox = "//form[@name='userForm']//input[@id='firstName']";
    private static final String lastNameTextBox = "//form[@name='userForm']//input[@id='lastName']";
    private static final String emailTextBox = "//form[@name='userForm']//input[@id='email']";
    private static final String monthDropdownBox = "//form[@name='userForm']//select[@id='birthMonth']";
    private static final String dayDropdownBox = "//form[@name='userForm']//select[@id='birthDay']";
    private static final String yearDropdownBox = "//form[@name='userForm']//select[@id='birthYear']";
    private static final String monthDropdownValue = "//form[@name='userForm']//select[@id='birthMonth']//option[text()='%s']";
    private static final String dayDropdownValue = "//form[@name='userForm']//select[@id='birthDay']//option[text()='%s']";
    private static final String yearDropdownValue = "//form[@name='userForm']//select[@id='birthYear']//option[text()='%s']";
    private static final String nextLocationButton = "//div[contains(@class, 'text-right')]//span[text()='Next: Location']";



    public UTestSignUpPage(WebDriver webDriver) {
        this.webTestDriver = new WebTestDriver(webDriver);
    }

    protected void inputFirstNameValue(String firstName) {
        webTestDriver.inputText(firstNameTextBox, firstName);
    }

    protected void inputLastNameValue(String lastName) {
        webTestDriver.inputText(lastNameTextBox, lastName);
    }

    protected void inputEmailValue(String email) {
        webTestDriver.inputText(emailTextBox, email);
    }

    protected void clickOnMonthDropdownBox() {
        webTestDriver.clickControl(monthDropdownBox);
    }

    protected void clickOnDayDropdownBox() {
        webTestDriver.clickControl(dayDropdownBox);
    }

    protected void clickOnYearDropdownBox() {
        webTestDriver.clickControl(yearDropdownBox);
    }

    protected void selectMonthDropDownValue(String monthValue) {
        String month = String.format(monthDropdownValue, monthValue);
        webTestDriver.clickControl(month);
    }

    protected void selectDayDropDownValue(int dayValue) {
        String date = String.format(dayDropdownValue, dayValue);
        webTestDriver.clickControl(date);
    }

    protected void selectYearDropDownValue(int yearValue) {
        String year = String.format(yearDropdownValue, yearValue);
        webTestDriver.inputText(yearDropdownBox, Integer.toString(yearValue));
        webTestDriver.clickControl(year);
    }

    protected void clickNextLocationButton() {
        webTestDriver.clickControl(nextLocationButton);
    }
}
