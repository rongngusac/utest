package com.qa.automation.utest.testCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;

public class UiTestCaseBase {
    public WebDriver driver;

    @BeforeSuite
    public void init() {
        String userDir = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", userDir + "\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    public void browseUrl(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
