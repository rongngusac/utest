package com.qa.automation.utest.testCase.SignUp;

import com.qa.automation.utest.pageObjectsAction.UTestAddAddressPageAction;
import com.qa.automation.utest.pageObjectsAction.UTestSignUpPageAction;
import com.qa.automation.utest.testCase.UiTestCaseBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC02_SignUpUTest_EmailFormat extends UiTestCaseBase {
    public UTestSignUpPageAction uTestSignUpPageAction;
    public UTestAddAddressPageAction uTestAddAddressPageAction;
    String FIRST_NAME = "Long";
    String LAST_NAME = "Nguyen";
    String EMAIL = "long@nguyen@utest.com";
    private String URL = "https://www.utest.com/signup/personal";

    @BeforeClass
    public void setup() {
        uTestSignUpPageAction = new UTestSignUpPageAction(getDriver());
    }

    @Test
    public void navigateToUTestPage() {
        browseUrl(URL);
    }

    @Test(dependsOnMethods = {"navigateToUTestPage"})
    public void inputFirstNameInfo() {
        uTestSignUpPageAction.inputFirstName(FIRST_NAME);
    }

    @Test(dependsOnMethods = {"navigateToUTestPage"})
    public void inputLastNameInfo() {
        uTestSignUpPageAction.inputLastName(LAST_NAME);
    }

    @Test(dependsOnMethods = {"navigateToUTestPage"})
    public void inputEmailInfo() {
        uTestSignUpPageAction.inputEmail(EMAIL);
    }
}
